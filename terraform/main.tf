#-----------------------------------#
#   Resource Group
#-----------------------------------#
resource "azurerm_resource_group" "rvi-rg" {
  name     = "rg-gourgandine-rvi"
  location = "francecentral"
}

############################################################################
#                                 Réseau                                   #
############################################################################

#-----------------------------------#
#   Virtual Network
#-----------------------------------#
resource "azurerm_virtual_network" "rvi-vn" {
  name                = "vn-gourgandine-rvi"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name
}

#-----------------------------------#
#   Subnets
#-----------------------------------#

# Subnet VMs
resource "azurerm_subnet" "rvi-subnet" {
  name                 = "subnet-gourgandine-rvi"
  resource_group_name  = azurerm_resource_group.rvi-rg.name
  virtual_network_name = azurerm_virtual_network.rvi-vn.name
  address_prefixes     = ["10.0.1.0/24"]
}

# Subnet Bastion
resource "azurerm_subnet" "rvi-subnet-bastion" {
  name                 = "AzureBastionSubnet"
  resource_group_name  = azurerm_resource_group.rvi-rg.name
  virtual_network_name = azurerm_virtual_network.rvi-vn.name
  address_prefixes     = ["10.0.2.0/24"]
}

#-----------------------------------#
#  Network Security Group
#-----------------------------------#
resource "azurerm_network_security_group" "rvi-nsg" {
  name                = "nsg-gourgandine-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name

  security_rule {
    name                       = "allow-simple-crud"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8088"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Network Security Group association with VMs' subnet
resource "azurerm_subnet_network_security_group_association" "rvi-nsg-association" {
  subnet_id                 = azurerm_subnet.rvi-subnet.id
  network_security_group_id = azurerm_network_security_group.rvi-nsg.id
}

#-----------------------------------#
#   Public IPs
#-----------------------------------#
# Public IP for Bastion
resource "azurerm_public_ip" "rvi-pip-bastion" {
  name                = "pip-gourgandine-bastion-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

# Public IP for Load Balancer
resource "azurerm_public_ip" "rvi-pip-lb" {
  name                = "pip-gourgandine-lb-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

# Public IP for NAT Gateway
resource "azurerm_public_ip" "rvi-pip-nat-gateway" {
  name                = "pip-gourgandine-nat-gateway-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

############################################################################
#                Bastion | NAT Gateway | Load Balancer                     #
############################################################################

#-----------------------------------#
#   Bastion
#-----------------------------------#
resource "azurerm_bastion_host" "rvi-bastion" {
  name                = "bastion-gourgandine-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name
  sku                 = "Standard"
  tunneling_enabled   = true
  ip_connect_enabled  = true
  ip_configuration {
    name                 = "configuration"
    subnet_id            = azurerm_subnet.rvi-subnet-bastion.id
    public_ip_address_id = azurerm_public_ip.rvi-pip-bastion.id
  }
}

#-----------------------------------#
#   NAT Gateway
#-----------------------------------#
resource "azurerm_nat_gateway" "rvi-nat-gateway" {
  name                = "nat-gateway-gourgandine-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name
}

# NAT Gateway association with its Public IP 
resource "azurerm_nat_gateway_public_ip_association" "rvi-nat-gateway-pip-association" {
  nat_gateway_id = azurerm_nat_gateway.rvi-nat-gateway.id
  public_ip_address_id   = azurerm_public_ip.rvi-pip-nat-gateway.id
}

# NAT Gateway association with VMs' subnet
resource "azurerm_subnet_nat_gateway_association" "rvi-nat-gateway-subnet-association" {
  subnet_id = azurerm_subnet.rvi-subnet.id
  nat_gateway_id = azurerm_nat_gateway.rvi-nat-gateway.id
}

#-----------------------------------#
#   Load Balancer
#-----------------------------------#
resource "azurerm_lb" "rvi-lb" {
  name                = "lb-gourgandine-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.rvi-pip-lb.id
  }
}

# Backend Pool
resource "azurerm_lb_backend_address_pool" "rvi-lb-backend-pool" {
  name                = "lb-backend-pool-gourgandine-rvi"
  loadbalancer_id     = azurerm_lb.rvi-lb.id
}

# Load Balancer probe (sonde)
resource "azurerm_lb_probe" "rvi-lb-probe" {
  name                = "tcp-probe"
  protocol            = "Tcp"
  port                = 8088
  loadbalancer_id     = azurerm_lb.rvi-lb.id
}

# Load Balancer Rule
resource "azurerm_lb_rule" "rvi-lb-rule" {
  name                            = "http-rule"
  loadbalancer_id                 = azurerm_lb.rvi-lb.id
  protocol                        = "Tcp"
  frontend_port                   = 80
  backend_port                    = 8088
  frontend_ip_configuration_name  = azurerm_lb.rvi-lb.frontend_ip_configuration[0].name
  backend_address_pool_ids        = [azurerm_lb_backend_address_pool.rvi-lb-backend-pool.id]
  probe_id                        = azurerm_lb_probe.rvi-lb-probe.id
  disable_outbound_snat           = true
}

############################################################################
#                           VMs | Database                                 #
############################################################################

#-----------------------------------#
#   Virtual Machine Scale Set
#-----------------------------------#
resource "azurerm_linux_virtual_machine_scale_set" "rvi-vmss" {
  name                  = "vmss-gourgandine-rvi"
  location              = azurerm_resource_group.rvi-rg.location
  resource_group_name   = azurerm_resource_group.rvi-rg.name
  sku                   = "Standard_DS1_v2"
  instances             = 2
  admin_username        = "adminusername"
  admin_password        = "Azerty123!"
  disable_password_authentication = false

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts-gen2"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  network_interface {
    name    = "nic-gourgandine-rvi"
    primary = true
    network_security_group_id = azurerm_network_security_group.rvi-nsg.id

    ip_configuration {
      name      = "internal"
      primary   = true
      subnet_id = azurerm_subnet.rvi-subnet.id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.rvi-lb-backend-pool.id]

    }
  }
}

#-----------------------------------#
#   Postgresql Server | Database
#-----------------------------------#
resource "azurerm_postgresql_server" "rvi-postgres-server" {
  name                = "postgres-server-rvi"
  location            = azurerm_resource_group.rvi-rg.location
  resource_group_name = azurerm_resource_group.rvi-rg.name

  administrator_login          = "adminlogin"
  administrator_login_password = "Azerty123!"

  sku_name            = "B_Gen5_1"
  storage_mb          = 5120
  version             = "11"
  ssl_minimal_tls_version_enforced = "TLSEnforcementDisabled"
  ssl_enforcement_enabled = false
}

resource "azurerm_postgresql_database" "rvi-database" {
  name                = "database-rvi"
  resource_group_name = azurerm_resource_group.rvi-rg.name
  server_name         = azurerm_postgresql_server.rvi-postgres-server.name
  charset             = "UTF8"
  collation           = "fr-FR"
}

# Firewall rule to let other resources access the database
resource "azurerm_postgresql_firewall_rule" "rvi-postgres-firewall-rule" {
  name                = "firewall-rule-rvi"
  resource_group_name = azurerm_resource_group.rvi-rg.name
  server_name         = azurerm_postgresql_server.rvi-postgres-server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

############################################################################
#                           Data | Output                                  #
############################################################################

#-----------------------------------#
#                Data
#-----------------------------------#
data "azurerm_virtual_machine_scale_set" "vmss-gourgandine-rvi" {
  name                = azurerm_linux_virtual_machine_scale_set.rvi-vmss.name
  resource_group_name = azurerm_resource_group.rvi-rg.name
}

#-----------------------------------#
#             Outputs
#-----------------------------------#
output "public_ip_address" {
  value = azurerm_public_ip.rvi-pip-lb.ip_address
}

output "vmss_instance_ids" {
  value = data.azurerm_virtual_machine_scale_set.vmss-gourgandine-rvi.instances[*].instance_id
}

output "bastion_name" {
  value = azurerm_bastion_host.rvi-bastion.name
}

output "resource_group_name"{
  value = azurerm_resource_group.rvi-rg.name
}

output "vmss_name"{
  value = azurerm_linux_virtual_machine_scale_set.rvi-vmss.name
}

output "DB_HOST"{
  value = azurerm_postgresql_server.rvi-postgres-server.fqdn
}

output "DB_NAME"{
  value = azurerm_postgresql_database.rvi-database.name
}

output "DB_USER"{
  value = azurerm_postgresql_server.rvi-postgres-server.administrator_login
}